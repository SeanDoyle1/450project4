/*********************************************************************
* Rice University Software Distribution License
*
* Copyright (c) 2010, Rice University
* All Rights Reserved.
*
* For a full description see the file named LICENSE.
*
*********************************************************************/

/* Author: Ioan Sucan */

#include <boost/bind.hpp>

#include <omplapp/config.h>
#include <ompl/tools/benchmark/Benchmark.h>
#include "rgrrt.h"
#include <ompl/control/planners/rrt/RRT.h>
#include <ompl/control/planners/kpiece/KPIECE1.h>

#include <ompl/base/samplers/UniformValidStateSampler.h>
#include <ompl/base/samplers/GaussianValidStateSampler.h>
#include <ompl/base/samplers/ObstacleBasedValidStateSampler.h>
#include <ompl/base/samplers/MaximizeClearanceValidStateSampler.h>

#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/control/spaces/RealVectorControlSpace.h>
#include <ompl/base/spaces/SO2StateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/control/SimpleSetup.h>
#include <ompl/control/ODESolver.h>

namespace ob = ompl::base;
namespace og = ompl::geometric;
namespace oc = ompl::control;

using namespace ompl;

// Define projections
class PendulumProjection : public ob::ProjectionEvaluator
{
    public:
    PendulumProjection(const ob::StateSpacePtr &space) : ob::ProjectionEvaluator(space)
    {
    }
    virtual unsigned int getDimension(void) const
    {
        return 2;
    }
    virtual void defaultCellSizes(void)
    {
        cellSizes_.resize(2);
        cellSizes_[0] = 0.1;
        cellSizes_[1] = 0.25;
    }
    virtual void project(const ob::State *state, ob::EuclideanProjection &projection) const
    {
        ob::State **comps = state->as<ob::CompoundState>()->components;
        const double angle = comps[0]->as<ob::SO2StateSpace::StateType>()->value;
        const double angVel = comps[1]->as<ob::RealVectorStateSpace::StateType>()->values[0];
        projection(0) = angle;
        projection(1) = angVel;
    }
};

class CarProjection : public ob::ProjectionEvaluator
{
    public:
    CarProjection(const ob::StateSpacePtr &space) : ob::ProjectionEvaluator(space)
    {
    }
    virtual unsigned int getDimension(void) const
    {
        return 2;
    }
    virtual void defaultCellSizes(void)
    {
        cellSizes_.resize(2);
        cellSizes_[0] = 0.1;
        cellSizes_[1] = 0.25;
    }
    virtual void project(const ob::State *state, ob::EuclideanProjection &projection) const
    {
        ob::State **comps = state->as<ob::CompoundState>()->components;
        ob::SE2StateSpace::StateType *se2Space = comps[0]->as<ob::SE2StateSpace::StateType>();
        const double vel = comps[1]->as<ob::RealVectorStateSpace::StateType>()->values[0];
        // vals = (x, y, theta), vel = (v)
        projection(0) = (se2Space->getX() + se2Space->getY()) / 2.0;
        projection(1) = (se2Space->getYaw() + vel) / 2.0;
    }
};

struct Rectangle
{
    // Coordinate of the lower left corner of the rectangle
    double x, y;
    // The width (x-axis extent) of the rectangle
    double width;
    // The height (y-axis extent) of the rectangle
    double height;
};

// This is our state validitiy checker for checking if our robot is within the bounds
bool isStateValidPendulum(const oc::SpaceInformation *si, const ob::State *state)
{
    /// cast the abstract state type to the type we expect
    // const ob::CompoundState *pendulumState = state->as<ob::CompoundState>();

    /// extract the first component of the state and cast it to what we expect
    // const ob::RealVectorStateSpace::StateType *pos = pendulumState->as<ob::RealVectorStateSpace::StateType>(1);

    /// check validity of state defined by angular velocity
    return si->satisfiesBounds(state);

    // return a value that is always true but uses the two variables we define, so we avoid compiler warnings
    // return si->satisfiesBounds(state) && (const void*)rot != (const void*)pos;
}

int flag1 = 1;
int flag2 = 1;

bool isStateValidCar(const oc::SpaceInformation *si, const std::vector<Rectangle>& obstacles, const ob::State *state)
{
    
    bool satBounds = si->satisfiesBounds(state);

    // cast the abstract state type to the type we expect
    const ob::CompoundState *carState = state->as<ob::CompoundState>();

    // extract the first component of the state and cast it to what we expect
    const ob::RealVectorStateSpace::StateType *pos = carState->as<ob::SE2StateSpace::StateType>(0)->as<ob::RealVectorStateSpace::StateType>(0);
    
    double xRob = pos->values[0];
    double yRob = pos->values[1];

    std::vector<Rectangle>::const_iterator it;
    double maxX, maxY, minX, minY;
    if(flag1 == 0) {
        std::cout << "Size of obstacles: " << obstacles.size() << std::endl;
        flag1 = 1;
    }
    for (it = obstacles.begin(); it != obstacles.end(); ++it) {
        minX = it->x;
        maxX = minX + it->width;
        minY = it->y;
        maxY = minY + it->height;

        if(flag2 == 0) {
            std::cout << "Bounds of obstacle: " << std::endl;
            std::cout << "(" << minX << "," << minY << ")" << std::endl;
            std::cout << "(" << minX << "," << maxY << ")" << std::endl;
            std::cout << "(" << maxX << "," << maxY << ")" << std::endl;
            std::cout << "(" << maxX << "," << minY << ")" << std::endl;
        }
        if ((minX <= xRob) && (maxX >= xRob) && (minY <= yRob) && (maxY >= yRob)) {
            return false;
        }
    }

    if (flag2 == 0) {
        flag2 = 1;
    }

    return satBounds;
}

void pendulumODE (const oc::ODESolver::StateType& q, const oc::Control* control, oc::ODESolver::StateType& qdot)
{
    const double *u = control->as<oc::RealVectorControlSpace::ControlType>()->values;

    // Zero out qdot
    qdot.resize (q.size (), 0);

    qdot[0] = q[1];
    qdot[1] = ((-9.81) * cos(q[0])) + u[0];
}

void carODE (const oc::ODESolver::StateType& q, const oc::Control* control, oc::ODESolver::StateType& qdot)
{
    const double *u = control->as<oc::RealVectorControlSpace::ControlType>()->values;
    const double theta = q[2];
    double carLength = 0.2;

    // Zero out qdot
    qdot.resize (q.size (), 0);

    qdot[0] = q[3] * cos(theta);
    qdot[1] = q[3] * sin(theta);
    qdot[2] = u[0];
    qdot[3] = u[1];
}

void PendulumPostIntegration (const ob::State* /*state*/, const oc::Control* /*control*/, const double /*duration*/, ob::State *result)
{
    // Normalize orientation between 0 and 2*pi
    ob::SO2StateSpace SO2;
    SO2.enforceBounds (result->as<ob::CompoundState>()->as<ob::SO2StateSpace::StateType>(0));
}

void CarPostIntegration (const ob::State* /*state*/, const oc::Control* /*control*/, const double /*duration*/, ob::State *result)
{
    // Normalize orientation between 0 and 2*pi
    ob::SO2StateSpace SO2;
    SO2.enforceBounds (result->as<ob::CompoundState>()->as<ob::SE2StateSpace::StateType>(0)->as<ob::SO2StateSpace::StateType>(1));
}

control::SimpleSetup planPendulum()
{
    // Step 1) Create the state (configuration) space for your system
    // For a robot that can translate in the plane, we can use R^2 directly
    // We also need to set bounds on R^2
    ob::StateSpacePtr pendulumSpace;

    ob::StateSpacePtr r1(new ob::RealVectorStateSpace(1));
    ob::RealVectorBounds bounds(1);
    bounds.setLow(-10);
    bounds.setHigh(10);

    r1->as<ob::RealVectorStateSpace>()->setBounds(bounds);

    ob::StateSpacePtr so2(new ob::SO2StateSpace());

    pendulumSpace = so2 + r1;

    pendulumSpace->registerProjection("pendulumProjection", ob::ProjectionEvaluatorPtr(new PendulumProjection(pendulumSpace)));


    // Set up the control space (R1)
    oc::ControlSpacePtr cr1(new oc::RealVectorControlSpace(pendulumSpace, 1));
    ob::RealVectorBounds controlBounds(1);
    controlBounds.setLow(-2);
    controlBounds.setHigh(2);

    cr1->as<oc::RealVectorControlSpace>()->setBounds(controlBounds);

    // Step 2) Create the SimpleSetup container for the motion planning problem.
    // this container ensures that everything is initialized properly before
    // trying to solve the motion planning problem.
    // ALWAYS USE SIMPLE SETUP!  There is no loss of functionality when using
    // this class.
    ompl::control::SimpleSetup ss(cr1);

    // Step 3) Setup the StateValidityChecker
    // This is a function that takes a state and returns whether the state is a
    // valid configuration of the system or not.  For geometric planning, this
    // is a collision checker.
    // Note, we are "binding" the obstacles to the state validity checker.  
    // The _1 notation is from boost::bind and indicates "the first argument" to the
    // function pointer.
    ss.setStateValidityChecker(boost::bind(&isStateValidPendulum, ss.getSpaceInformation().get(), _1));


    // Use the ODESolver to propagate the system.  Call KinematicCarPostIntegration
    // when integration has finished to normalize the orientation values.
    oc::ODESolverPtr odeSolver(new oc::ODEBasicSolver<> (ss.getSpaceInformation(), &pendulumODE));
    ss.setStatePropagator(oc::ODESolver::getStatePropagator(odeSolver, &PendulumPostIntegration));

    // Step 4) Specify the start and goal states
    // ScopedState creates the correct state instance for the state space
    ompl::base::ScopedState<> start(pendulumSpace);
    start[0] = -M_PI/2;
    start[1] = 0;

    ompl::base::ScopedState<> goal(pendulumSpace);
    goal[0] = M_PI/2;
    goal[1] = 0;

    // set the start and goal states
    ss.setStartAndGoalStates(start, goal);

    return ss;
}

control::SimpleSetup planCar(const std::vector<Rectangle>& obstacles)
{
    // Step 1) Create the state (configuration) space for your system
    // In this instance, we will plan for a square of side length 0.3
    // that both translates and rotates in the plane.
    // The state space can be easily composed of simpler state spaces
    ob::StateSpacePtr carSpace;

    // Set up R1
    ob::StateSpacePtr r1(new ob::RealVectorStateSpace(1));
    ob::RealVectorBounds r1bounds(1);
    r1bounds.setLow(-10);
    r1bounds.setHigh(10);
    r1->as<ob::RealVectorStateSpace>()->setBounds(r1bounds);


    ob::StateSpacePtr se2(new ob::SE2StateSpace());
    // We need to set bounds on SE2
    ob::RealVectorBounds bounds(2);
    bounds.setLow(-10); // x and y have a minimum of -10
    bounds.setHigh(10); // x and y have a maximum of 10

    // Cast the se2 pointer to the derived type, then set the bounds
    se2->as<ob::SE2StateSpace>()->setBounds(bounds);

    // Set the carSpace pointer to the combination of SE2 and R1
    carSpace = se2 + r1;

    carSpace->registerProjection("carProjection", ob::ProjectionEvaluatorPtr(new CarProjection(carSpace)));

    // Set up the control space
    oc::ControlSpacePtr cr2(new oc::RealVectorControlSpace(carSpace, 2));
    ob::RealVectorBounds controlBounds(2);
    controlBounds.setLow(-2);
    controlBounds.setHigh(2);

    cr2->as<oc::RealVectorControlSpace>()->setBounds(controlBounds);

    // Step 2) Create the SimpleSetup container for the motion planning problem.
    // this container ensures that everything is initialized properly before
    // trying to solve the motion planning problem using OMPL.
    // ALWAYS USE SIMPLE SETUP!  There is no loss of functionality when using
    // this class.
    ompl::control::SimpleSetup ss(cr2);
    ss.getSpaceInformation().get()->setPropagationStepSize(0.3);

    // Step 3) Setup the StateValidityChecker
    // This is a function that takes a state and returns whether the state is a
    // valid configuration of the system or not.  For geometric planning, this
    // is a collision checker

    // Note, we are "binding" the side length, 0.3, and the obstacles to the
    // state validity checker.  The _1 notation is from boost::bind and indicates
    // "the first argument" to the function pointer.
    ss.setStateValidityChecker(boost::bind(&isStateValidCar, ss.getSpaceInformation().get(), obstacles, _1));

    // Use the ODESolver to propagate the system.  Call KinematicCarPostIntegration
    // when integration has finished to normalize the orientation values.
    oc::ODESolverPtr odeSolver(new oc::ODEBasicSolver<> (ss.getSpaceInformation(), &carODE));
    ss.setStatePropagator(oc::ODESolver::getStatePropagator(odeSolver, &CarPostIntegration));

    // Step 4) Specify the start and goal states
    // ScopedState creates the correct state instance for the state space
    // You can index into the components of the state easily with ScopedState
    // The indexes correspond to the order that the StateSpace components were
    // added into the StateSpace
    ompl::base::ScopedState<> start(carSpace);
    start[0] = -9.75;
    start[1] = -9.5;
    start[2] = 0.0;
    start[3] = 0.0;

    ompl::base::ScopedState<> goal(carSpace);
    goal[0] = 9.4;
    goal[1] = 9.35;
    goal[2] = 0.0;
    goal[3] = 0.0;

    // set the start and goal states
    ss.setStartAndGoalStates(start, goal);

    return ss;
}

void buildObstacles(std::vector<Rectangle> *obstacles)
{
    Rectangle obstacle;

    // Build borders
    obstacle.x = -10;
    obstacle.y = -10.01;
    obstacle.width = 20;
    obstacle.height = 0.01;
    obstacles->push_back(obstacle);

    obstacle.x = -10.01;
    obstacle.y = -10;
    obstacle.width = 0.01;
    obstacle.height = 20;
    obstacles->push_back(obstacle);

    obstacle.x = -10;
    obstacle.y = 10;
    obstacle.width = 20;
    obstacle.height = 0.01;
    obstacles->push_back(obstacle);

    obstacle.x = 10;
    obstacle.y = -10;
    obstacle.width = 0.01;
    obstacle.height = 20;
    obstacles->push_back(obstacle);

    // std::cout << "Building Checkerboard obstacles" << std::endl;
    for(int i = -4; i < 5; i++) {
        for(int j = -4; j < 5; j++) {
            obstacle.x = (double) 2*i;
            obstacle.y = (double) 2*j;
            obstacle.width = 1.5;
            obstacle.height = 1.5;
            obstacles->push_back(obstacle);
        }
    }

}

base::ValidStateSamplerPtr allocUniformStateSampler(const base::SpaceInformation *si)
{
    return base::ValidStateSamplerPtr(new base::UniformValidStateSampler(si));
}

// void benchmark0(std::string& benchmark_name, control::SimpleSetup& setup,
//                 double& runtime_limit, double& memory_limit, int& run_count)
// {
//     benchmark_name = std::string("cubicles");
//     std::string robot_fname = std::string(OMPLAPP_RESOURCE_DIR) + "/3D/cubicles_robot.dae";
//     std::string env_fname = std::string(OMPLAPP_RESOURCE_DIR) + "/3D/cubicles_env.dae";
//     setup.setRobotMesh(robot_fname.c_str());
//     setup.setEnvironmentMesh(env_fname.c_str());

//     base::ScopedState<base::SE3StateSpace> start(setup.getSpaceInformation());
//     start->setX(-4.96);
//     start->setY(-40.62);
//     start->setZ(70.57);
//     start->rotation().setIdentity();

//     base::ScopedState<base::SE3StateSpace> goal(start);
//     goal->setX(200.49);
//     goal->setY(-40.62);
//     goal->setZ(70.57);
//     goal->rotation().setIdentity();

//     setup.setStartAndGoalStates(start, goal);
//     setup.getSpaceInformation()->setStateValidityCheckingResolution(0.01);
//     setup.getSpaceInformation()->setValidStateSamplerAllocator(&allocUniformStateSampler);
//     setup.setup();

//     std::vector<double> cs(3);
//     cs[0] = 35; cs[1] = 35; cs[2] = 35;
//     setup.getStateSpace()->getDefaultProjection()->setCellSizes(cs);

//     runtime_limit = 10.0;
//     memory_limit  = 10000.0; // set high because memory usage is not always estimated correctly
//     run_count     = 25;
// }

// void benchmark1(std::string& benchmark_name, control::SimpleSetup& setup,
//                 double& runtime_limit, double& memory_limit, int& run_count)
// {
//     benchmark_name = std::string("Twistycool");
//     std::string robot_fname = std::string(OMPLAPP_RESOURCE_DIR) + "/3D/Twistycool_robot.dae";
//     std::string env_fname = std::string(OMPLAPP_RESOURCE_DIR) + "/3D/Twistycool_env.dae";
//     setup.setRobotMesh(robot_fname.c_str());
//     setup.setEnvironmentMesh(env_fname.c_str());

//     base::ScopedState<base::SE3StateSpace> start(setup.getSpaceInformation());
//     start->setX(270.);
//     start->setY(160.);
//     start->setZ(-200.);
//     start->rotation().setIdentity();

//     base::ScopedState<base::SE3StateSpace> goal(start);
//     goal->setX(270.);
//     goal->setY(160.);
//     goal->setZ(-400.);
//     goal->rotation().setIdentity();

//     base::RealVectorBounds bounds(3);
//     bounds.setHigh(0,350.);
//     bounds.setHigh(1,250.);
//     bounds.setHigh(2,-150.);
//     bounds.setLow(0,200.);
//     bounds.setLow(1,75.);
//     bounds.setLow(2,-450.);
//     setup.getStateSpace()->as<base::SE3StateSpace>()->setBounds(bounds);

//     setup.setStartAndGoalStates(start, goal);
//     setup.getSpaceInformation()->setStateValidityCheckingResolution(0.01);

//     runtime_limit = 60.0;
//     memory_limit  = 10000.0; // set high because memory usage is not always estimated correctly
//     run_count     = 25;
// }

void preRunEvent(const base::PlannerPtr& /*planner*/)
{
}

void postRunEvent(const base::PlannerPtr& /*planner*/, tools::Benchmark::RunProperties& /*run*/)
{
}

int main(int argc, char **argv)
{

    std::vector<Rectangle> obstacles;
    buildObstacles(&obstacles);
    control::SimpleSetup setup = planPendulum();
    std::string benchmark_name = "grid_env";
    double runtime_limit = 10.0, memory_limit = 10000.0;
    int run_count = 20;
    int benchmark_id = argc > 1 ? ((argv[1][0] - '0') % 2) : 0;

    if (benchmark_id==0) {
        setup = planCar(obstacles);
        std::cout << "Benchmarking Car" << std::endl;
    } else {
        std::cout << "Benchmarking Pendulum" << std::endl;
    }

    // create the benchmark object and add all the planners we'd like to run
    tools::Benchmark::Request request(runtime_limit, memory_limit, run_count);
    tools::Benchmark b(setup, benchmark_name);

    // optionally set pre & pos run events
    b.setPreRunEvent(boost::bind(&preRunEvent, _1));
    b.setPostRunEvent(boost::bind(&postRunEvent, _1, _2));

    b.addPlanner(base::PlannerPtr(new control::RGRRT(setup.getSpaceInformation())));
    b.addPlanner(base::PlannerPtr(new control::RRT(setup.getSpaceInformation())));
    base::Planner* kPiecePlanner = new control::KPIECE1(setup.getSpaceInformation());
    if (benchmark_id == 0)
        kPiecePlanner->as<control::KPIECE1>()->setProjectionEvaluator("carProjection");
    else
        kPiecePlanner->as<control::KPIECE1>()->setProjectionEvaluator("pendulumProjection");
    b.addPlanner(base::PlannerPtr(kPiecePlanner));

    // run all planners with a uniform valid state sampler on the benchmark problem
    // setup.getSpaceInformation()->setValidStateSamplerAllocator(&allocUniformStateSampler);
    b.setExperimentName(benchmark_name + "_uniform_sampler");
    b.benchmark(request);
    b.saveResultsToFile();

    return 0;
}
