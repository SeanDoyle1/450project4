#!/usr/bin/env python
from mpl_toolkits.mplot3d import Axes3D, art3d
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import sys
from math import sin, cos

# Plot a path in SO2xR1 with no obstacles
def plotPendulum(path):
    fig = plt.figure()
    ax = fig.gca()

    # Plotting the path
    X = [p[0] for p in path]
    Y = [p[1] for p in path]
    ax.plot(X, Y)

    plt.show()

def plotCar(path, plot):
    fig = plt.figure()
    ax = fig.gca()


    if int(plot) == 0:
        # Drawing the checkerboard pattern
        for i in range(-4, 5):
            for j in range(-4, 5):
                corner = (2*i, 2*j)
                ax.add_patch(patches.Polygon([corner,(corner[0]+1.5,corner[1]),(corner[0]+1.5,corner[1]+1.5),(corner[0],corner[1]+1.5)], fill=True, color='0.20'))
    elif int(plot) == 1:
        # Drawing the zigzag pattern
        for i in range(-2,2):
            corner1 = (-2, i + 0.2)
            corner2 = (-2, i + 0.3)
            corner3 = (1.5, i + 0.3)
            corner4 = (1.5, i + 0.2)
            ax.add_patch(patches.Polygon([corner1, corner2, corner3, corner4], fill=True, color='0.20'))

            corner1 = (-1.5, i+0.7)
            corner2 = (-1.5, i+0.8)
            corner3 = (2, i+0.8)
            corner4 = (2, i+0.7)
            ax.add_patch(patches.Polygon([corner1, corner2, corner3, corner4], fill=True, color='0.20'))

    # Plotting the path (reference point)
    X = [p[0] for p in path]
    Y = [p[1] for p in path]
    ax.plot(X, Y)

    # Plotting the actual box
    boxVert = [[-0.15, -0.15], [0.15, -0.15], [0.15, 0.15], [-0.15, 0.15], [-0.15, -0.15]]

    for p in path:
        x = []
        y = []
        for v in boxVert:
            x.append(v[0] * cos(p[2]) - v[1] * sin(p[2]) + p[0])
            y.append(v[0] * sin(p[2]) + v[1] * cos(p[2]) + p[1])
        ax.plot(x, y, 'k')

    plt.axis([-10,10,-10,10])
    plt.show()


# Read the cspace definition and the path from filename
def readPath(filename):
    lines = [line.rstrip() for line in open(filename) if len(line.rstrip()) > 0]

    if len(lines) == 0:
        print "Ain't nuthin in this file"
        sys.exit(1)

    cspace = lines[0].strip()
    if (cspace != 'SO2xR1' and cspace != 'SE2xR1'):
        print "Unknown c-space identifier: " + cspace
        sys.exit(1)

    data = [[float(x) for x in line.split(' ')] for line in lines[1:]]
    return cspace, data

if __name__ == '__main__':
    if len(sys.argv) > 2:
        filename = sys.argv[1]
        pattern = sys.argv[2]
    else:
        filename = 'path.txt'

    cspace, path = readPath(filename)

    if cspace == 'SO2xR1':
        plotPendulum(path)
    elif cspace == 'SE2xR1':
        plotCar(path, pattern)
