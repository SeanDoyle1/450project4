#include <iostream>
#include <fstream>
#include <limits>
#include <valarray>
#define _USE_MATH_DEFINES
#include <cmath>

#include <boost/bind.hpp>

#include <ompl/config.h>
// Including SimpleSetup.h will pull in MOST of what you need to plan
#include <ompl/control/SimpleSetup.h>
#include <ompl/control/ODESolver.h>
// Except for the state space definitions and any planners
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/control/spaces/RealVectorControlSpace.h>
#include <ompl/base/spaces/SO2StateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/control/planners/rrt/RRT.h>
#include <ompl/control/planners/kpiece/KPIECE1.h>
#include "rgrrt.h"

// Define short names for namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;
namespace oc = ompl::control;

// Define projections
class PendulumProjection : public ob::ProjectionEvaluator
{
    public:
    PendulumProjection(const ob::StateSpacePtr &space) : ob::ProjectionEvaluator(space)
    {
    }
    virtual unsigned int getDimension(void) const
    {
        return 2;
    }
    virtual void defaultCellSizes(void)
    {
        cellSizes_.resize(2);
        cellSizes_[0] = 0.1;
        cellSizes_[1] = 0.25;
    }
    virtual void project(const ob::State *state, ob::EuclideanProjection &projection) const
    {
        ob::State **comps = state->as<ob::CompoundState>()->components;
        const double angle = comps[0]->as<ob::SO2StateSpace::StateType>()->value;
        const double angVel = comps[1]->as<ob::RealVectorStateSpace::StateType>()->values[0];
        projection(0) = angle;
        projection(1) = angVel;
    }
};

class CarProjection : public ob::ProjectionEvaluator
{
    public:
    CarProjection(const ob::StateSpacePtr &space) : ob::ProjectionEvaluator(space)
    {
    }
    virtual unsigned int getDimension(void) const
    {
        return 2;
    }
    virtual void defaultCellSizes(void)
    {
        cellSizes_.resize(2);
        cellSizes_[0] = 0.1;
        cellSizes_[1] = 0.25;
    }
    virtual void project(const ob::State *state, ob::EuclideanProjection &projection) const
    {
        ob::State **comps = state->as<ob::CompoundState>()->components;
        ob::SE2StateSpace::StateType *se2Space = comps[0]->as<ob::SE2StateSpace::StateType>();
        const double vel = comps[1]->as<ob::RealVectorStateSpace::StateType>()->values[0];
        // vals = (x, y, theta), vel = (v)
        projection(0) = (se2Space->getX() + se2Space->getY()) / 2.0;
        projection(1) = (se2Space->getYaw() + vel) / 2.0;
    }
};


struct Rectangle
{
    // Coordinate of the lower left corner of the rectangle
    double x, y;
    // The width (x-axis extent) of the rectangle
    double width;
    // The height (y-axis extent) of the rectangle
    double height;
};

// This is our state validitiy checker for checking if our robot is within the bounds
bool isStateValidPendulum(const oc::SpaceInformation *si, const ob::State *state)
{
    /// cast the abstract state type to the type we expect
    // const ob::CompoundState *pendulumState = state->as<ob::CompoundState>();

    /// extract the first component of the state and cast it to what we expect
    // const ob::RealVectorStateSpace::StateType *pos = pendulumState->as<ob::RealVectorStateSpace::StateType>(1);

    /// check validity of state defined by angular velocity
    return si->satisfiesBounds(state);

    // return a value that is always true but uses the two variables we define, so we avoid compiler warnings
    // return si->satisfiesBounds(state) && (const void*)rot != (const void*)pos;
}

int flag1 = 1;
int flag2 = 1;

bool isStateValidCar(const oc::SpaceInformation *si, const std::vector<Rectangle>& obstacles, const ob::State *state)
{
    
    bool satBounds = si->satisfiesBounds(state);

    // cast the abstract state type to the type we expect
    const ob::CompoundState *carState = state->as<ob::CompoundState>();

    // extract the first component of the state and cast it to what we expect
    const ob::RealVectorStateSpace::StateType *pos = carState->as<ob::SE2StateSpace::StateType>(0)->as<ob::RealVectorStateSpace::StateType>(0);
    
    double xRob = pos->values[0];
    double yRob = pos->values[1];

    std::vector<Rectangle>::const_iterator it;
    double maxX, maxY, minX, minY;
    if(flag1 == 0) {
        std::cout << "Size of obstacles: " << obstacles.size() << std::endl;
        flag1 = 1;
    }
    for (it = obstacles.begin(); it != obstacles.end(); ++it) {
        minX = it->x;
        maxX = minX + it->width;
        minY = it->y;
        maxY = minY + it->height;

        if(flag2 == 0) {
            std::cout << "Bounds of obstacle: " << std::endl;
            std::cout << "(" << minX << "," << minY << ")" << std::endl;
            std::cout << "(" << minX << "," << maxY << ")" << std::endl;
            std::cout << "(" << maxX << "," << maxY << ")" << std::endl;
            std::cout << "(" << maxX << "," << minY << ")" << std::endl;
        }
        if ((minX <= xRob) && (maxX >= xRob) && (minY <= yRob) && (maxY >= yRob)) {
            return false;
        }
    }

    if (flag2 == 0) {
        flag2 = 1;
    }

    return satBounds;
}

void pendulumODE (const oc::ODESolver::StateType& q, const oc::Control* control, oc::ODESolver::StateType& qdot)
{
    const double *u = control->as<oc::RealVectorControlSpace::ControlType>()->values;

    // Zero out qdot
    qdot.resize (q.size (), 0);

    qdot[0] = q[1];
    qdot[1] = ((-9.81) * cos(q[0])) + u[0];
}

void carODE (const oc::ODESolver::StateType& q, const oc::Control* control, oc::ODESolver::StateType& qdot)
{
    const double *u = control->as<oc::RealVectorControlSpace::ControlType>()->values;
    const double theta = q[2];
    double carLength = 0.2;

    // Zero out qdot
    qdot.resize (q.size (), 0);

    qdot[0] = q[3] * cos(theta);
    qdot[1] = q[3] * sin(theta);
    qdot[2] = u[0];
    qdot[3] = u[1];
}

void PendulumPostIntegration (const ob::State* /*state*/, const oc::Control* /*control*/, const double /*duration*/, ob::State *result)
{
    // Normalize orientation between 0 and 2*pi
    ob::SO2StateSpace SO2;
    SO2.enforceBounds (result->as<ob::CompoundState>()->as<ob::SO2StateSpace::StateType>(0));
}

void CarPostIntegration (const ob::State* /*state*/, const oc::Control* /*control*/, const double /*duration*/, ob::State *result)
{
    // Normalize orientation between 0 and 2*pi
    ob::SO2StateSpace SO2;
    SO2.enforceBounds (result->as<ob::CompoundState>()->as<ob::SE2StateSpace::StateType>(0)->as<ob::SO2StateSpace::StateType>(1));
}

void planPendulum(int plan)
{
    // Step 1) Create the state (configuration) space for your system
    // For a robot that can translate in the plane, we can use R^2 directly
    // We also need to set bounds on R^2
    ob::StateSpacePtr pendulumSpace;

    ob::StateSpacePtr r1(new ob::RealVectorStateSpace(1));
    ob::RealVectorBounds bounds(1);
    bounds.setLow(-10);
    bounds.setHigh(10);

    r1->as<ob::RealVectorStateSpace>()->setBounds(bounds);

    ob::StateSpacePtr so2(new ob::SO2StateSpace());

    pendulumSpace = so2 + r1;


    // Set up the control space (R1)
    oc::ControlSpacePtr cr1(new oc::RealVectorControlSpace(pendulumSpace, 1));
    ob::RealVectorBounds controlBounds(1);
    controlBounds.setLow(-2);
    controlBounds.setHigh(2);

    cr1->as<oc::RealVectorControlSpace>()->setBounds(controlBounds);

    // Step 2) Create the SimpleSetup container for the motion planning problem.
    // this container ensures that everything is initialized properly before
    // trying to solve the motion planning problem.
    // ALWAYS USE SIMPLE SETUP!  There is no loss of functionality when using
    // this class.
    ompl::control::SimpleSetup ss(cr1);

    // Step 3) Setup the StateValidityChecker
    // This is a function that takes a state and returns whether the state is a
    // valid configuration of the system or not.  For geometric planning, this
    // is a collision checker.
    // Note, we are "binding" the obstacles to the state validity checker.  
    // The _1 notation is from boost::bind and indicates "the first argument" to the
    // function pointer.
    ss.setStateValidityChecker(boost::bind(&isStateValidPendulum, ss.getSpaceInformation().get(), _1));


    // Use the ODESolver to propagate the system.  Call KinematicCarPostIntegration
    // when integration has finished to normalize the orientation values.
    oc::ODESolverPtr odeSolver(new oc::ODEBasicSolver<> (ss.getSpaceInformation(), &pendulumODE));
    ss.setStatePropagator(oc::ODESolver::getStatePropagator(odeSolver, &PendulumPostIntegration));

    // Step 4) Specify the start and goal states
    // ScopedState creates the correct state instance for the state space
    ompl::base::ScopedState<> start(pendulumSpace);
    start[0] = -M_PI/2;
    start[1] = 0;

    ompl::base::ScopedState<> goal(pendulumSpace);
    goal[0] = M_PI/2;
    goal[1] = 0;

    // set the start and goal states
    ss.setStartAndGoalStates(start, goal);

    // Step 5) (optional) Specify a planning algorithm to use
    switch(plan)
    {
        case 1:
        {
            std::cout << "Working with RRT" << std::endl;
            ob::PlannerPtr planner(new oc::RRT(ss.getSpaceInformation()));
            ss.setPlanner(planner);
            break;
        }
        case 2:
        {
            std::cout << "Working with KPIECE1" << std::endl;
            ob::PlannerPtr planner(new oc::KPIECE1(ss.getSpaceInformation()));
            pendulumSpace->registerProjection("pendulumProjection", ob::ProjectionEvaluatorPtr(new PendulumProjection(pendulumSpace)));
            planner->as<oc::KPIECE1>()->setProjectionEvaluator("pendulumProjection");
            ss.setPlanner(planner);
            break;
        }
        case 3:
        {
            std::cout << "Working with RG-RRT" << std::endl;
            ob::PlannerPtr planner(new oc::RGRRT(ss.getSpaceInformation()));
            ss.setPlanner(planner);
            break;
        }
    }
    // Step 6) Attempt to solve the problem within the given time (seconds)
    ompl::base::PlannerStatus solved = ss.solve(10.0);

    if (solved)
    {

        // print the path to screen
        std::cout << "Found solution:" << std::endl;
        ss.getSolutionPath().asGeometric().printAsMatrix(std::cout);

        // print path to file
        std::ofstream fout("path.txt");
        fout << "SO2xR1" << std::endl;
        ss.getSolutionPath().asGeometric().printAsMatrix(fout);
        fout.close();
    }
    else
        std::cout << "No solution found" << std::endl;
}

void planCar(const std::vector<Rectangle>& obstacles, int plan)
{
    // Step 1) Create the state (configuration) space for your system
    // In this instance, we will plan for a square of side length 0.3
    // that both translates and rotates in the plane.
    // The state space can be easily composed of simpler state spaces
    ob::StateSpacePtr carSpace;

    // Set up R1
    ob::StateSpacePtr r1(new ob::RealVectorStateSpace(1));
    ob::RealVectorBounds r1bounds(1);
    r1bounds.setLow(-10);
    r1bounds.setHigh(10);
    r1->as<ob::RealVectorStateSpace>()->setBounds(r1bounds);


    ob::StateSpacePtr se2(new ob::SE2StateSpace());
    // We need to set bounds on SE2
    ob::RealVectorBounds bounds(2);
    bounds.setLow(-10); // x and y have a minimum of -10
    bounds.setHigh(10); // x and y have a maximum of 10

    // Cast the se2 pointer to the derived type, then set the bounds
    se2->as<ob::SE2StateSpace>()->setBounds(bounds);

    // Set the carSpace pointer to the combination of SE2 and R1
    carSpace = se2 + r1;

    // Set up the control space
    oc::ControlSpacePtr cr2(new oc::RealVectorControlSpace(carSpace, 2));
    ob::RealVectorBounds controlBounds(2);
    controlBounds.setLow(-2);
    controlBounds.setHigh(2);

    cr2->as<oc::RealVectorControlSpace>()->setBounds(controlBounds);

    // Step 2) Create the SimpleSetup container for the motion planning problem.
    // this container ensures that everything is initialized properly before
    // trying to solve the motion planning problem using OMPL.
    // ALWAYS USE SIMPLE SETUP!  There is no loss of functionality when using
    // this class.
    ompl::control::SimpleSetup ss(cr2);
    ss.getSpaceInformation().get()->setPropagationStepSize(0.2);

    // Step 3) Setup the StateValidityChecker
    // This is a function that takes a state and returns whether the state is a
    // valid configuration of the system or not.  For geometric planning, this
    // is a collision checker

    // Note, we are "binding" the side length, 0.3, and the obstacles to the
    // state validity checker.  The _1 notation is from boost::bind and indicates
    // "the first argument" to the function pointer.
    ss.setStateValidityChecker(boost::bind(&isStateValidCar, ss.getSpaceInformation().get(), obstacles, _1));

    // Use the ODESolver to propagate the system.  Call KinematicCarPostIntegration
    // when integration has finished to normalize the orientation values.
    oc::ODESolverPtr odeSolver(new oc::ODEBasicSolver<> (ss.getSpaceInformation(), &carODE));
    ss.setStatePropagator(oc::ODESolver::getStatePropagator(odeSolver, &CarPostIntegration));

    // Step 4) Specify the start and goal states
    // ScopedState creates the correct state instance for the state space
    // You can index into the components of the state easily with ScopedState
    // The indexes correspond to the order that the StateSpace components were
    // added into the StateSpace
    ompl::base::ScopedState<> start(carSpace);
    start[0] = -9.75;
    start[1] = -9.5;
    start[2] = 0.0;
    start[3] = 0.0;

    ompl::base::ScopedState<> goal(carSpace);
    goal[0] = 9.4;
    goal[1] = 9.35;
    goal[2] = 0.0;
    goal[3] = 0.0;

    // set the start and goal states
    ss.setStartAndGoalStates(start, goal);

    // Step 5) (optional) Specify a planning algorithm to use
    switch(plan)
    {
        case 1:
        {
            ob::PlannerPtr planner(new oc::RRT(ss.getSpaceInformation()));
            ss.setPlanner(planner);
            break;
        }
        case 2:
        {
            ob::PlannerPtr planner(new oc::KPIECE1(ss.getSpaceInformation()));
            carSpace->registerProjection("carProjection", ob::ProjectionEvaluatorPtr(new CarProjection(carSpace)));
            planner->as<oc::KPIECE1>()->setProjectionEvaluator("carProjection");
            ss.setPlanner(planner);
            break;
        }
        case 3:
        {
            ob::PlannerPtr planner(new oc::RGRRT(ss.getSpaceInformation()));
            ss.setPlanner(planner);
            break;
        }
    }

    // Step 6) Attempt to solve the problem within the given time (seconds)
    ompl::base::PlannerStatus solved = ss.solve(10.0);

    if (solved)
    {
        // print the path to screen
        std::cout << "Found solution:" << std::endl;
        ss.getSolutionPath().asGeometric().printAsMatrix(std::cout);

        // print path to file
        std::ofstream fout("path.txt");
        fout << "SE2xR1" << std::endl;
        ss.getSolutionPath().asGeometric().printAsMatrix(fout);
        fout.close();
    }
    else
        std::cout << "No solution found" << std::endl;
}

void buildObstacles(std::vector<Rectangle> *obstacles)
{
    Rectangle obstacle;

    // Build borders
    obstacle.x = -10;
    obstacle.y = -10.01;
    obstacle.width = 20;
    obstacle.height = 0.01;
    obstacles->push_back(obstacle);

    obstacle.x = -10.01;
    obstacle.y = -10;
    obstacle.width = 0.01;
    obstacle.height = 20;
    obstacles->push_back(obstacle);

    obstacle.x = -10;
    obstacle.y = 10;
    obstacle.width = 20;
    obstacle.height = 0.01;
    obstacles->push_back(obstacle);

    obstacle.x = 10;
    obstacle.y = -10;
    obstacle.width = 0.01;
    obstacle.height = 20;
    obstacles->push_back(obstacle);

    // std::cout << "Building Checkerboard obstacles" << std::endl;
    for(int i = -4; i < 5; i++) {
        for(int j = -4; j < 5; j++) {
            obstacle.x = (double) 2*i;
            obstacle.y = (double) 2*j;
            obstacle.width = 1.5;
            obstacle.height = 1.5;
            obstacles->push_back(obstacle);
        }
    }

}

int main(int, char **)
{
    int choice, planner;
    std::vector<Rectangle> obstacles;

    do
    {
        std::cout << "Plan for: "<< std::endl;
        std::cout << " (1) A pendulum" << std::endl;
        std::cout << " (2) A car" << std::endl;

        std::cin >> choice;
    } while (choice < 1 || choice > 2);

    do
    {
        std::cout << "Planner: "<< std::endl;
        std::cout << " (1) RRT" << std::endl;
        std::cout << " (2) KPIECE1" << std::endl;
        std::cout << " (3) RG-RRT" << std::endl;

        std::cin >> planner;
    } while (planner < 1 || planner > 3);


    switch(choice)
    {
        case 1:
            planPendulum(planner);
            break;
        case 2:
            buildObstacles(&obstacles);
            planCar(obstacles, planner);
            break;
    }
    return 0;
}
