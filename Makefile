OMPL_DIR = /usr
CXXFLAGS = -O2 # change to -g when debugging code
INCLUDE_FLAGS = -I${OMPL_DIR}/include
LD_FLAGS = -L${OMPL_DIR}/lib -lompl -lompl_app_base -lompl_app -lboost_program_options -Wl,-rpath ${OMPL_DIR}/lib -lboost_system
CXX=c++


RRTPlanning: RRTPlanning.o rgrrt.o
	$(CXX) $(CXXFLAGS) $(INCLUDE_FLAGS) -o RRTPlanning RRTPlanning.o rgrrt.o $(LD_FLAGS)

RGRRTBenchmark: rgrrt.o
	$(CXX) $(CXXFLAGS) $(INCLUDE_FLAGS) -o RGRRTBenchmark RGRRTBenchmark.cpp rgrrt.o $(LD_FLAGS)


clean:
	rm *.o

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $(INCLUDE_FLAGS) $< -o $@